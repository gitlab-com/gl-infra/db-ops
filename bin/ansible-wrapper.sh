#!/bin/bash
set -e

dir=$(pwd)
ansible_extra_opts=("-i $dir/inventory/$DB_OPS_ENV.yml")

if [[ "$CI" ]]; then
  mkdir -p /root/.ssh
  chmod 700 /root/.ssh

  echo "$SSH_PUBLIC_KEY" > /root/.ssh/id_rsa.pub
  echo "$SSH_PRIVATE_KEY" > /root/.ssh/id_rsa
  echo "$SSH_CONFIG" > /root/.ssh/config

  chmod 644 /root/.ssh/id_rsa.pub
  chmod 600 /root/.ssh/id_rsa
  chmod 600 /root/.ssh/config
  ansible_extra_opts+=("--user=$SSH_USERNAME" "--key-file=/root/.ssh/id_rsa")
fi

export ANSIBLE_CONFIG="$dir/ansible.cfg"

if [[ "$CHECKMODE" == "true" ]]; then
  ansible_extra_opts+=("--check")
fi

if [[ "$ANSIBLE_STRATEGY" == "mitogen_linear" ]]; then
  if [[ "$CI" ]]; then
    # The path below matches that of the built Docker image
    export ANSIBLE_STRATEGY_PLUGINS=/usr/local/lib/python3.7/site-packages/ansible_mitogen/plugins/strategy
  else
    export ANSIBLE_STRATEGY_PLUGINS="${VIRTUAL_ENV}"/lib/python3.7/site-packages/ansible_mitogen/plugins/strategy
  fi
fi

time=$(date +%s)
ansible-playbook ${ansible_extra_opts[@]} "$@"
